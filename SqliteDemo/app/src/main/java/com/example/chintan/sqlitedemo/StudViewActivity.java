package com.example.chintan.sqlitedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;

public class StudViewActivity extends AppCompatActivity
{
    RecyclerView recyclerView;
    StudDataBase stud_db;
    ArrayList<Student> customer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_view);
        stud_db=new StudDataBase(StudViewActivity.this);
        try
        {
            if(stud_db!=null)
            {
                stud_db.openDB();
                customer=stud_db.getAllRecords();

            }
        }
        catch (Exception ex)
        {
            Toast.makeText(StudViewActivity.this," "+ex,Toast.LENGTH_LONG).show();
        }
        finally
        {
            if(stud_db!=null)
            {
                stud_db.closeDB();
            }
        }

        // recycler view code starts here
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(StudViewActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        StudentAdapter adapter=new StudentAdapter(StudViewActivity.this,customer);

        recyclerView.setAdapter(adapter);
    }

}
