package com.example.chintan.sqlitedemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Chintan on 3/20/2017.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder> implements View.OnClickListener
{
    Context context;
    ArrayList<Student> studentArrayList;

    public StudentAdapter(Context context, ArrayList<Student> studentArrayList)
    {
        this.context = context;
        this.studentArrayList = studentArrayList;
    }

    @Override
    public StudentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StudentAdapter.MyViewHolder holder, int position)
    {
        Student studentdata = studentArrayList.get(position);
        holder.username.setText(studentdata.getUsername().toString());
        holder.password.setText(studentdata.getPassword().toString());
        holder.address.setText(studentdata.getAddress().toString());
        holder.email.setText(studentdata.getEmail().toString());
        holder.mobile.setText(studentdata.getMobile().toString());
    }

    @Override
    public int getItemCount()
    {
        return studentArrayList.size();
    }

    @Override
    public void onClick(View v)
    {

    }

    static class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView username,password,address,email,mobile;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            username = (TextView) itemView.findViewById(R.id.cardview_username);
            password = (TextView) itemView.findViewById(R.id.cardview_password);
            address = (TextView) itemView.findViewById(R.id.cardview_address);
            email = (TextView) itemView.findViewById(R.id.cardview_email);
            mobile = (TextView) itemView.findViewById(R.id.cardview_mobile);

        }
    }
}
