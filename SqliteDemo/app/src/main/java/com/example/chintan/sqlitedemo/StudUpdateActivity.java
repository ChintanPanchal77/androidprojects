package com.example.chintan.sqlitedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StudUpdateActivity extends AppCompatActivity
{
    EditText id,name,password,address,email,mobile;
    Button idbtn,updatebtn;
    StudDataBase Stud_db;
    String values[];
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_update);

        id=(EditText)findViewById(R.id.update_id);

        name=(EditText)findViewById(R.id.update_name);
        password=(EditText)findViewById(R.id.update_password);
        address= (EditText) findViewById(R.id.update_address);
        email=(EditText)findViewById(R.id.update_email);
        mobile=(EditText)findViewById(R.id.update_phone);

        name.setVisibility(View.INVISIBLE);
        password.setVisibility(View.INVISIBLE);
        address.setVisibility(View.INVISIBLE);
        email.setVisibility(View.INVISIBLE);
        mobile.setVisibility(View.INVISIBLE);
        //button
        idbtn=(Button)findViewById(R.id.update_getbtn);
        updatebtn=(Button)findViewById(R.id.update_updatebtn);

        Stud_db=new StudDataBase(StudUpdateActivity.this);

        idbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag1=validateID(id);

                if(flag1==true)
                {
                    try
                    {
                        Stud_db.openDB();
                        values=Stud_db.getUpdateValues(Integer.parseInt(id.getText().toString()));
                        if(values!=null)
                        {
                            name.setVisibility(View.VISIBLE);
                            password.setVisibility(View.VISIBLE);
                            address.setVisibility(View.VISIBLE);
                            email.setVisibility(View.VISIBLE);
                            mobile.setVisibility(View.VISIBLE);
                            name.setText(values[0].toString());
                            password.setText(values[1].toString());
                            address.setText(values[2].toString());
                            email.setText(values[3].toString());
                            mobile.setText(values[4].toString());

                        }
                    }
                    catch (Exception ex)
                    {
                        Toast.makeText(StudUpdateActivity.this,"No Such ID found in Table",Toast.LENGTH_LONG).show();
                    }
                    finally
                    {

                    }
                }
            }
        });

        updatebtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int result;
                boolean flag1=validateData(name,email,mobile);
                if(flag1==true)
                {
                    try
                    {
                        Stud_db.openDB();
                        result=Stud_db.UpdateStudent(Integer.parseInt(id.getText().toString()),name.getText().toString(),password.getText().toString(),address.getText().toString(),email.getText().toString(),mobile.getText().toString());
                        if(result>0)
                        {
                            Toast.makeText(StudUpdateActivity.this,"Sucessfully updated Record for ID ",Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if(Stud_db!=null)
                        {
                            Stud_db.closeDB();
                        }
                    }
                }
            }
        });
    }

    private boolean validateData(EditText name, EditText email, EditText mobile)
    {
        String pattern="[a-zA-Z0-9._]@[a-zA-Z].[a-zA-Z]";
        Pattern p= Pattern.compile(pattern);
        Matcher m=p.matcher(email.getText().toString());
        int namelen=name.getText().length();

        int mobilelen=mobile.getText().length();

        if(namelen<=0)
        {
            name.setError("Enter Proper name");
            name.requestFocus();
            return false;
        }
        if(!(m.find()))
        {
            email.setError("Enter Proper email");
            email.requestFocus();
            return false;
        }
        if(!(mobilelen==10))
        {
            mobile.setError("Enter Proper mobile");
            mobile.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateID(EditText id)
    {
        if(id.getText().length()<=0)
        {
            return false;
        }
        return true;
    }
}
