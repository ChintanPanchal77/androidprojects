package com.example.chintan.sqlitedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    EditText username,password,repwd,address,email,mobile;
    StudDataBase stud_db;
    Button btnSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stud_db = new StudDataBase(MainActivity.this);
        username = (EditText)findViewById(R.id.EditText1);
        password = (EditText)findViewById(R.id.EditText2);
        repwd = (EditText)findViewById(R.id.EditText3);
        address = (EditText)findViewById(R.id.EditText4);
        email = (EditText)findViewById(R.id.EditText5);
        mobile = (EditText)findViewById(R.id.EditText6);
        btnSubmit = (Button)findViewById(R.id.button1);

        Button button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(this);

        Button button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(this);

        Button button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(this);

        password.addTextChangedListener((new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                repwd.setText(password.getText());
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        }));

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0)
            {
                boolean flag= validate();

                long insertid;

                if(flag == true)
                {
                    try {
                        if (stud_db != null)
                        {
                            stud_db.openDB();

                            insertid = stud_db.insertIntoStudent(username.getText().toString(),password.getText().toString(), address.getText().toString(),email.getText().toString(), mobile.getText().toString());
                            if (insertid >= 0)
                            {
                                Log.e("username", " " + username.getText().toString());
                                Log.e("password", " " + password.getText().toString());
                                Log.e("address", " " + address.getText().toString());
                                Log.e("email", " " + email.getText().toString());
                                Log.e("mobile", " " + mobile.getText().toString());

                                Toast.makeText(MainActivity.this, "Record Inserted in DB at position :" + insertid, Toast.LENGTH_LONG).show();

                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Toast.makeText(MainActivity.this, " " + ex, Toast.LENGTH_LONG).show();
                    }
                    finally
                    {
                        if (stud_db != null)
                        {
                            stud_db.closeDB();

                        }
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        validate();

        if(v.getId() == R.id.button1)
        {
            Toast.makeText(this, "Validate Sucessfully", Toast.LENGTH_SHORT).show();
        }
        else if(v.getId() == R.id.button2)
        {
            MainActivity.this.finish();
        }
        else if(v.getId() == R.id.button3)
        {
            username.setText("");
            password.setText("");
            repwd.setText("");
            address.setText("");
            email.setText("");
            mobile.setText("");
        }
    }

    public boolean validate()
    {
        boolean valid = true;

        if (username.getText().toString().length() == 0 || username.getText().toString().length() < 5 || username.getText().toString().length() > 20)
        {
            //Toast.makeText(getApplicationContext(), " Invalid Name", Toast.LENGTH_LONG).show();
            username.setError("Name cannot be Blank");
            valid = false;
        }
        else
        {
            username.setError(null);
        }

        if (password.getText().toString().isEmpty() || password.getText().toString().length() < 4 || password.getText().toString().length() > 10) {
            password.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        }
        else
        {
            password.setError(null);
        }

        if (repwd.getText().toString().isEmpty() || repwd.getText().toString().length() < 4 || repwd.getText().toString().length() > 10) {
            repwd.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        }
        else
        {
            repwd.setError(null);
        }

        if(((password.getText().toString().equals(repwd)) ) || (password.getText().toString().length()<=0) || (repwd.getText().toString().length()<=0) )
        {
            Toast.makeText(this," Password Match Fail", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "Password Match Success", Toast.LENGTH_SHORT).show();
        }

        if (address.getText().toString().isEmpty() || address.getText().toString().length() > 40 ) {
            address.setError("Enter Valid Adress!");
            valid = false;
        }
        else
        {
            address.setError(null);
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches() || email.getText().toString().length() == 0)
        {
            //Validation for Invalid Email Address
            Toast.makeText(this, "Invalid Email", Toast.LENGTH_LONG).show();
            email.setError("Invalid Email");
            valid = false;
        }
        else
        {
            email.setError(null);
        }

        if (mobile.getText().toString().isEmpty() || mobile.getText().toString().length() < 7 || mobile.getText().toString().length() > 10)
        {
            mobile.setError("between 7 and 10 numeric characters");
            valid = false;
        }
        else
        {
            mobile.setError(null);
        }

        return valid;
    }
}