package com.example.chintan.sqlitedemo;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainIndexActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_index);
        SQLiteDatabase sqLiteDatabase;
        sqLiteDatabase=openOrCreateDatabase("Stud_db",SQLiteDatabase.CREATE_IF_NECESSARY,null);

        Button display=(Button)findViewById(R.id.display);
        Button insert=(Button)findViewById(R.id.insert);
        Button update=(Button)findViewById(R.id.update);
        Button delete=(Button)findViewById(R.id.delete);

        final Intent dis=new Intent(getApplicationContext(),StudViewActivity.class);
        final Intent ins=new Intent(getApplicationContext(),MainActivity.class);
        final Intent upd=new Intent(getApplicationContext(),StudUpdateActivity.class);
        final Intent del=new Intent(getApplicationContext(),StudDeleteActivity.class);

        display.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(dis);
            }
        });

        insert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(ins);
            }
        });

        update.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(upd);
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(del);
            }
        });
    }
}
