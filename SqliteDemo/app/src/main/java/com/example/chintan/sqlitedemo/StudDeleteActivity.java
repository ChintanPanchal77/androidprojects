package com.example.chintan.sqlitedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StudDeleteActivity extends AppCompatActivity
{
    EditText id;
    Button btndelete;
    StudDataBase stud_db;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_delete);

        id=(EditText)findViewById(R.id.deleteid);
        btndelete=(Button)findViewById(R.id.btndelete);
        stud_db=new StudDataBase(StudDeleteActivity.this);
        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean result=false;
                boolean flag=validate(id);
                if(flag==true)
                {
                    try
                    {
                        stud_db.openDB();
                        result=stud_db.deleteFromDB(Integer.parseInt(id.getText().toString()));
                        if(result==true)
                        {
                            Toast.makeText(StudDeleteActivity.this," Row deleted Successfully",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(StudDeleteActivity.this,"No record with mentioned ID found",Toast.LENGTH_LONG).show();
                        }
                    }
                    catch(Exception ex)
                    {
                        Toast.makeText(StudDeleteActivity.this," "+ex,Toast.LENGTH_LONG).show();
                    }
                    finally
                    {
                        if(stud_db!=null)
                        {
                            stud_db.closeDB();
                        }
                    }
                }
            }
        });
    }

    private boolean validate(EditText id)
    {
        if(id.getText().length()<=0)
        {
            id.setError("Enter id");
            id.requestFocus();
            return false;
        }
        return true;
    }
}
