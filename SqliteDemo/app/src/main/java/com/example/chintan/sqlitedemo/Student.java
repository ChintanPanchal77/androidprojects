package com.example.chintan.sqlitedemo;

/**
 * Created by Chintan on 3/15/2017.
 */

public class Student
{
    private String username,address,password,email,mobile;

    public Student(String username, String address, String password, String email, String mobile)
    {
        this.username = username;
        this.address = address;
        this.password = password;
        this.email = email;
        this.mobile = mobile;
    }

    public String getUsername() {return username;}

    public void setUsername(String username) {this.username = username;}

    public String getAddress() {return address;}

    public void setAddress(String address) {this.address = address;}

    public String getPassword() {return password;}

    public void setPassword(String password) {this.password = password;}

    public String getEmail() {return email;}

    public void setEmail(String email) {this.email = email;}

    public String getMobile() {return mobile;}

    public void setMobile(String mobile) {this.mobile = mobile;}

}
