package com.example.chintan.sqlitedemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;
import java.util.ArrayList;

/**
 * Created by Chintan on 3/15/2017.
 */

public class StudDataBase extends SQLiteOpenHelper
{
    private Context context;
    private SQLiteDatabase sqLiteDatabase;
    ArrayList<Student> studentslist = new ArrayList<Student>();

    private final static String MY_DB = "stud_db";
    private final static int MY_DB_VERSION = 1;

    //My Student Table
    private final static String STUD_TBL = "Student";
    private final static String STUD_ID = "_id";
    private final static String STUD_UNAME = "username";
    private final static String STUD_PASSWORD = "password";
    private final static String STUD_ADDRESS = "address";
    private final static String STUD_EMAILID = "email";
    private final static String STUD_MOBILE = "mobile";


    public StudDataBase(Context context)
    {
        super(context, MY_DB, null, MY_DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        //create all your tables over here
        String StudeCreatreQuery = "Create table " + STUD_TBL + " ( " + STUD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + STUD_UNAME + " TEXT, " + STUD_PASSWORD + " TEXT, " + STUD_ADDRESS + " TEXT, " + STUD_EMAILID + " TEXT, " + STUD_MOBILE + " TEXT)";
        db.execSQL(StudeCreatreQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }

    public void openDB()
    {
        sqLiteDatabase = new StudDataBase(context).getWritableDatabase();
        Toast.makeText(context,"DB OPEN",Toast.LENGTH_SHORT);
    }

    public void closeDB()
    {
        Toast.makeText(context,"DB CLOSED",Toast.LENGTH_SHORT);
        if (sqLiteDatabase != null && sqLiteDatabase.isOpen())
        {
            sqLiteDatabase.close();
        }
    }

    public boolean deleteFromDB(int id)
    {
        return sqLiteDatabase.delete(STUD_TBL, STUD_ID + "=" + id,null) > 0;
    }

    public ArrayList<Student> getAllRecords()
    {
        Log.e("StudDataBase", "In method fetch All Records");

        Cursor cursor = sqLiteDatabase.query(STUD_TBL, null, null, null, null, null, null, null);
        Log.e("In method", "After Cursor creation ");

        while (cursor.moveToNext())
        {
            Log.e("in while loop" , "cursor iteration ");
            String id = cursor.getString(cursor.getColumnIndex(STUD_ID));
            String username = cursor.getString(cursor.getColumnIndex(STUD_UNAME));
            String password = cursor.getString(cursor.getColumnIndex(STUD_PASSWORD));
            String address = cursor.getString(cursor.getColumnIndex(STUD_ADDRESS));
            String email = cursor.getString(cursor.getColumnIndex(STUD_EMAILID));
            String mobile = cursor.getString(cursor.getColumnIndex(STUD_MOBILE));
            Log.e("id", " " +id);
            Log.e("username", " "+username);
            Log.e("password", " " +password);
            Log.e("address", " " +address);
            Log.e("email", " " +email);
            Log.e("mobile", " " +mobile);

            studentslist.add(new Student("User Name :"+"\n"+username,"Password :"+"\n"+password,"Address :"+"\n"+address,"Email Id :"+"\n"+email,"Mobile :"+"\n"+mobile));
        }
        cursor.close();

        return studentslist;
    }

    public long insertIntoStudent(String username, String password, String address, String email, String mobile)
    {
        ContentValues cv = new ContentValues();
        cv.put(STUD_UNAME, username);
        cv.put(STUD_PASSWORD, password);
        cv.put(STUD_ADDRESS, address);
        cv.put(STUD_EMAILID, email);
        cv.put(STUD_MOBILE , mobile);
        long _id = sqLiteDatabase.insert(STUD_TBL, null, cv);
        return _id;
    }

    public int UpdateStudent(int id, String username, String password, String address, String email, String mobile)
    {
        ContentValues cv = new ContentValues();
        cv.put(STUD_UNAME, username);
        cv.put(STUD_PASSWORD, password);
        cv.put(STUD_ADDRESS,address);
        cv.put(STUD_EMAILID, email);
        cv.put(STUD_MOBILE, mobile);
        int result = sqLiteDatabase.update(STUD_TBL, cv, STUD_ID + "=" + id,null);
        return result;
    }
    public String[] getUpdateValues(int id)

    {
        String data[] = new String[5];
        Cursor cursor = sqLiteDatabase.query(STUD_TBL, null, STUD_ID + "=" +id, null, null, null, null, null);
        while (cursor.moveToNext())
        {
            String username = cursor.getString(cursor.getColumnIndex(STUD_UNAME));
            String password = cursor.getString(cursor.getColumnIndex(STUD_PASSWORD));
            String address = cursor.getString(cursor.getColumnIndex(STUD_ADDRESS));
            String email = cursor.getString(cursor.getColumnIndex(STUD_EMAILID));
            String mobile = cursor.getString(cursor.getColumnIndex(STUD_MOBILE));
            Log.e("username"," "+username);
            Log.e("password"," "+password);
            Log.e("address"," " + address);
            Log.e("email"," "+email);
            Log.e("mobile"," "+mobile);
            data[0] = username;
            data[1]=password;
            data[2]=address;
            data[3]=email;
            data[4]=mobile;
        }
        cursor.close();

        return data;
    }
}
