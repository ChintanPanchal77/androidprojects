package com.example.chintan.fragmentdemo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by Chintan on 5/10/2017.
 */

public class CallLogsAdapter extends RecyclerView.Adapter<CallLogsAdapter.ViewHolder>
{
    private Context context;
    private ArrayList<CalllogsData> list;

    public CallLogsAdapter(Context context, ArrayList<CalllogsData> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public CallLogsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = ((Activity) (context)).getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_call_logs_card, parent, false);
        ViewHolder viewHolder = new CallLogsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CallLogsAdapter.ViewHolder holder, int position)
    {
        CalllogsData callLogsData = list.get(position);
        holder.name.setText(callLogsData.getName());
        holder.number.setText(callLogsData.getNumber());
        // holder.type.setImageResource(callLogsData.getType());
        Glide.with(context).load(callLogsData.getPhoto()).into(holder.image);
        if (callLogsData.getType() == CallLog.Calls.MISSED_TYPE) {
            holder.type.setImageResource(R.drawable.missed);
        } else if (callLogsData.getType() == CallLog.Calls.INCOMING_TYPE) {
            holder.type.setImageResource(R.drawable.incoming);
        } else if (callLogsData.getType() == CallLog.Calls.OUTGOING_TYPE) {
            holder.type.setImageResource(R.drawable.outgoing);
        }
        holder.callcard.setTag(R.id.callcard, position);
        holder.callcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout layout = (LinearLayout) v;
                int p = (int) layout.getTag(R.id.callcard);
                CalllogsData call = list.get(p);
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+call.getNumber()));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) context,new String[]{Manifest.permission.CALL_PHONE},1);
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name;
        private TextView number;
        private ImageView type;
        private ImageView image;
        private LinearLayout callcard;
        public ViewHolder(View itemView)
        {
            super(itemView);
            name =(TextView) itemView.findViewById(R.id.callname);
            number =(TextView) itemView.findViewById(R.id.callnumber);
            image =(ImageView) itemView.findViewById(R.id.callimage);
            type =(ImageView) itemView.findViewById(R.id.calltype);
            callcard = (LinearLayout) itemView.findViewById(R.id.callcard);



        }
    }
}
