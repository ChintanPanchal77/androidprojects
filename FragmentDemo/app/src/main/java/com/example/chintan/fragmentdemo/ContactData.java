package com.example.chintan.fragmentdemo;

/**
 * Created by Chintan on 3/23/2017.
 */

public class ContactData
{
    public String ContactImage;
    public String ContactName;
    public String ContactNumber;
    public String ContactEmail;

    public ContactData(String contactImage, String contactName, String contactNumber, String contactEmail)
    {
        ContactImage = contactImage;
        ContactName = contactName;
        ContactNumber = contactNumber;
        ContactEmail = contactEmail;
    }

    public String getContactImage()
    {
        return ContactImage;
    }

    public void setContactImage(String contactImage)
    {
        ContactImage = contactImage;
    }

    public String getContactName()
    {
        return ContactName;
    }

    public void setContactName(String contactName)
    {
        ContactName = contactName;
    }

    public String getContactNumber()
    {
        return ContactNumber;
    }

    public void setContactNumber(String contactNumber)
    {
        ContactNumber = contactNumber;
    }

    public String getContactEmail()
    {
        return ContactEmail;
    }

    public void setContactEmail(String contactEmail)
    {
        ContactEmail = contactEmail;
    }
}
