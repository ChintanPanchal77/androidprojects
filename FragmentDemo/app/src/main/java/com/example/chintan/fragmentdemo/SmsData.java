package com.example.chintan.fragmentdemo;

/**
 * Created by Chintan on 3/23/2017.
 */

public class SmsData
{
    int id;
    public String sender;
    public String message;
    public String date;

    public SmsData(int id, String sender, String message, String date)
    {
        this.id = id;
        this.sender = sender;
        this.message = message;
        this.date = date;
    }

    public int getId()
    {
        return id;
    }

    public String getSender()
    {
        return sender;
    }

    public String getMessage()
    {
        return message;
    }

    public String getDate()
    {
        return date;
    }
}