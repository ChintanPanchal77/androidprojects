package com.example.chintan.fragmentdemo;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import java.util.ArrayList;

public class CallLogsDisplay extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_logs_display);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_call_logs_display, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private Context context;
        private ArrayList<CalllogsData> allList;
        private ArrayList<CalllogsData> missedlist;
        private ArrayList<CalllogsData> outgoinglist;
        private ArrayList<CalllogsData> incominglist;

        public void gelAllData()
        {
            context = getContext();
            ContentResolver contentResolver = context.getContentResolver();
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Cursor cursor = contentResolver.query(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DEFAULT_SORT_ORDER);
            String name,number,photo,duration,date;
            int type;
            allList= new ArrayList<>();
            missedlist= new ArrayList<>();
            outgoinglist= new ArrayList<>();
            incominglist= new ArrayList<>();
            while (cursor.moveToNext())
            {
                type = Integer.parseInt(cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE)));
                name = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
                number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                photo = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_PHOTO_ID));
                date = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DATE));
                duration = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DURATION));
                allList.add(new CalllogsData(name,number,photo,date,duration,type));

                if(type== CallLog.Calls.MISSED_TYPE)
                {
                    missedlist.add(new CalllogsData(name,number,photo,date,duration,type));
                }
                else if(type==CallLog.Calls.OUTGOING_TYPE)
                {
                    outgoinglist.add(new CalllogsData(name,number,photo,date,duration,type));
                }
                else if(type==CallLog.Calls.INCOMING_TYPE)
                {
                    incominglist.add(new CalllogsData(name,number,photo,date,duration,type));
                }

            }
            cursor.close();


        }

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.fragment_call_logs_display, container, false);
            gelAllData();
            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.rvcalls);
            recyclerView.hasFixedSize();
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(layoutManager);
            CallLogsAdapter adapter;

            if ( getArguments().getInt(ARG_SECTION_NUMBER)==1)
            {
                adapter = new CallLogsAdapter(context,allList);
                recyclerView.setAdapter(adapter);
            }
            else if ( getArguments().getInt(ARG_SECTION_NUMBER)==2)
            {
                adapter = new CallLogsAdapter(context,incominglist);
                recyclerView.setAdapter(adapter);

            }
            else if ( getArguments().getInt(ARG_SECTION_NUMBER)==3)
            {
                adapter = new CallLogsAdapter(context,outgoinglist);
                recyclerView.setAdapter(adapter);
            }
            else if ( getArguments().getInt(ARG_SECTION_NUMBER)==4)
            {
                adapter = new CallLogsAdapter(context,missedlist);
                recyclerView.setAdapter(adapter);
            }


            /* TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
           */ return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show  total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "All Calls";
                case 1:
                    return "Incoming Calls";
                case 2:
                    return "Outgoing Calls";
                case 3:
                    return "Missed Calls";
            }
            return null;
        }
    }
}
