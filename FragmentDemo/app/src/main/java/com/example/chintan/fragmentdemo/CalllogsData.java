package com.example.chintan.fragmentdemo;

/**
 * Created by Chintan on 5/10/2017.
 */

public class CalllogsData
{
    private String name,number,photo,date,duration;
    private int type;

    public CalllogsData(String name, String number, String photo, String date, String duration, int type)
    {
        this.name = name;
        this.number = number;
        this.photo = photo;
        this.date = date;
        this.duration = duration;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
