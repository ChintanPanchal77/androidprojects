package com.example.chintan.broadcastreceivers;

import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private Button btnSms,btncalllog,btnCamera,btnGallery, btnwhatsapp,btnWhatsappChat, btnFacebook;
    private Button btnCalender, btnContactList, btnSettings, btnGPS, btnAlertDialog;
    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSms = (Button) findViewById(R.id.button5);
        btnSms.setOnClickListener(this);

        btncalllog = (Button) findViewById(R.id.button6);
        btncalllog.setOnClickListener(this);

        btnCamera = (Button) findViewById(R.id.button7);
        btnCamera.setOnClickListener(this);

        btnGallery = (Button) findViewById(R.id.button8);
        btnGallery.setOnClickListener(this);

        btnwhatsapp = (Button) findViewById(R.id.button9);
        btnwhatsapp.setOnClickListener(this);

        btnWhatsappChat = (Button) findViewById(R.id.button10);
        btnWhatsappChat.setOnClickListener(this);

        btnFacebook = (Button) findViewById(R.id.button12);
        btnFacebook.setOnClickListener(this);

        btnCalender = (Button) findViewById(R.id.button13);
        btnCalender.setOnClickListener(this);

        btnContactList = (Button) findViewById(R.id.button15);
        btnContactList.setOnClickListener(this);


        btnSettings = (Button) findViewById(R.id.button16);
        btnSettings.setOnClickListener(this);

        btnGPS = (Button) findViewById(R.id.button17);
        btnGPS.setOnClickListener(this);

        btnAlertDialog =(Button) findViewById(R.id.button18);
        btnAlertDialog.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.button5) {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage("8849188391", null, "hi chintan", null, null);
                Toast.makeText(getApplicationContext(), "SMS Sent!",
                        Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),
                        "SMS faild, please try again later!",
                        Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }else if(v.getId()==R.id.button6){
            Intent showCallLog = new Intent();
            showCallLog.setAction(Intent.ACTION_VIEW);
            showCallLog.setType(CallLog.Calls.CONTENT_TYPE);
            startActivity(showCallLog);

        }else if(v.getId()==R.id.button7){

            Intent intent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            File f = new File(android.os.Environment
                    .getExternalStorageDirectory(), "IMG_20161216_125130_1483123304040.jpg");
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(f));

            startActivityForResult(intent,
                    CAMERA_REQUEST);

        }else if(v.getId()==R.id.button8){

            Intent pictureActionIntent = null;

            pictureActionIntent = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(
                    pictureActionIntent,
                    GALLERY_PICTURE);

        }else if(v.getId()==R.id.button9){
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            sendIntent.setType("text/plain");
            sendIntent.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(sendIntent, ""));
            startActivity(sendIntent);
        }else if(v.getId()==R.id.button10){

            Uri uri = Uri.parse("smsto:" + "8980441817");
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);

            i.putExtra(Intent.EXTRA_TEXT, "Hello");
            i.setPackage("com.whatsapp");
            startActivity(i);


        }else if(v.getId()==R.id.button12){

            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/426253597411506"));
                startActivity(intent);
            } catch(Exception e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com")));
            }

        }else if(v.getId()==R.id.button13){

            long startMillis = 5000;
            Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
            builder.appendPath("time");
            ContentUris.appendId(builder, startMillis);
            Intent intent = new Intent(Intent.ACTION_VIEW)
                    .setData(builder.build());
            startActivity(intent);

        }
        else if(v.getId()==R.id.button15){

            Intent contacts = new Intent(Intent.ACTION_VIEW, ContactsContract.Contacts.CONTENT_URI);

            // Starting the activity
            startActivity(contacts);

        }else if(v.getId()==R.id.button16){
            startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);

        }else if(v.getId()==R.id.button17){

            // Uri uri = Uri.parse("google.streetview:cbll-46.414382,10.013988");
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setPackage("com.google.android.apps.maps");
            startActivity(i);
        }else if(v.getId()==R.id.button18){

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("This is title");
            builder.setMessage("This is message");
            builder.setPositiveButton("Yes lets do it", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainActivity.this.finish();
                }
            });

            builder.setNegativeButton("nope", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();


        }
    }
}
