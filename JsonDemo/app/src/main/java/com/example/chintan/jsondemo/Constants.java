package com.example.chintan.jsondemo;

/**
 * Created by Chintan on 3/26/2017.
 */

public class Constants
{
    public static final String ARGS1="route";
    public static final String ARGS2="customer_id";
    public static final String ARGS3="email";
    public static final String KEY1="order_id";
    public static final String KEY2="name";
    public static final String KEY3="status";
    public static final String KEY4="date_added";
    public static final String KEY5="products";
    public static final String KEY6="total";
    public static final String KEY7="href";
}
