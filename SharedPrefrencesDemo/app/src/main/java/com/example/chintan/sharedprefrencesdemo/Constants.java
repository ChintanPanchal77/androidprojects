package com.example.chintan.sharedprefrencesdemo;

/**
 * Created by Chintan on 3/27/2017.
 */

public class Constants
{
    public static final String NAME="name";
    public static final String EMAIL="email";
    public static final String NUMBER="number";
}
